**drills_part_1**

1. Create the following directocd mkry structure. (Create empty files where necessary)
	
	mkdir hello
	cd hello
	pwd
	/home/sagarn/hello
	mkdir five
	cd five
	pwd
	/home/sagarn/hello/five
	mkdir six
	cd six
	touch c.txt
	mkdir seven
	pwd 
	/home/sagarn/hello/five/six/seven
	touch error.log
	
	--------------------------------------------------------------------------
	
-> To go back to hello directory and create one directory

	cd ..
	cd ..
	cd ..
	mkdir one
	ls 
	five one
	cd one
	pwd
	/home/sagarn/hello/one
	touch a.txt
	touch b.txt
	ls
	a.txt b.txt
	mkdir two
	cd two
	touch d.txt
	mkdir three
	ls 
	d.txt three
	cd three
	touch e.txt
	mkdir four 
	ls
	e.txt four
	cd four
	touch access.log
	pwd
	/home/sagarn/hello/one/two/three/four
	
	----------------------------------------------------------------------------

2. Delete all the files with the .log extension

	cd /home/sagarn/hello

-> To find files .log extension.

	find . -name "*.log" -type f
	./one/two/three/four/access.log
	./five/six/seven/error.log

-> To delete all files under current directory with .log extension.
	
	find .-name "*.log" -type f -delete
	
-> To check if the files have been deleted.

	find . -name "*.log" -type f

	----------------------------------------------------------------------------

3.Add the following content to a.txt
	
	cat > one/a.txt

-> typing the content and exiting by pressing ctrl+d.

Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.

	----------------------------------------------------------------------------

4. Deleting the directory named five.

	rm -rf five
	ls
	one
